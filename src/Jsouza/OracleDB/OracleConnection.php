<?php

namespace Jsouza\OracleDB;

use Illuminate\Database\Connection;
use Jsouza\OracleDB\Schema\OracleBuilder;
use Jsouza\OracleDB\Query\Processors\OracleProcessor;
use Doctrine\DBAL\Driver\OCI8\Driver as DoctrineDriver;
use Jsouza\OracleDB\Query\Grammars\OracleGrammar as QueryGrammer;
use Jsouza\OracleDB\Schema\Grammars\OracleGrammar as SchemaGrammer;
use PDO;

class OracleConnection extends Connection
{
    /**
     * Get a schema builder instance for the connection.
     *
     * @return \Illuminate\Database\Schema\OracleBuilder
     */
    public function getSchemaBuilder()
    {
        if (is_null($this->schemaGrammar)) {
            $this->useDefaultSchemaGrammar();
        }

        return new OracleBuilder($this);
    }

    /**
     * Get the default query grammar instance.
     *
     * @return \Jsouza\OracleDB\Query\Grammars\OracleGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new QueryGrammer);
    }

    /**
     * Get the default schema grammar instance.
     *
     * @return \Jsouza\OracleDB\Schema\Grammars\OracleGrammar
     */
    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new SchemaGrammer);
    }

    /**
     * Get the default post processor instance.
     *
     * @return \Jsouza\OracleDB\Query\Processors\OracleProcessor
     */
    protected function getDefaultPostProcessor()
    {
        return new OracleProcessor;
    }

    /**
     * Get the Doctrine DBAL driver.
     *
     * @return \Doctrine\DBAL\Driver\OCI8\Driver
     */
    protected function getDoctrineDriver()
    {
        return new DoctrineDriver;
    }

    /**
     * Bind values to their parameters in the given statement.
     *
     * @param  \PDOStatement $statement
     * @param  array  $bindings
     * @return void
     */
    public function bindValues($statement, $bindings)
    {
        foreach ($bindings as $key => $value) {
            $statement->bindValue(
                $key,
                $value,
                is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR
            );
        }
    }


}
